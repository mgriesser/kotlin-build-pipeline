package org.hsmith.medium.kotlin_build_pipeline

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

internal class CalculatorTest {
    @Test
    fun `GIVEN working calculator WHEN calculating sum THEN sum is correct`() {
        val calculator = WorkingCalculator()
        assertEquals(10, calculator.sum(5, 5),
            "Calculator should return the correct result")
    }

    @Test
    @Disabled // Remove this attribute to run the test - this test will fail
    fun `GIVEN broken calculator WHEN calculating sum THEN sum is correct`() {
        val calculator = BrokenCalculator()
        assertEquals(10, calculator.sum(5, 5),
            "Calculator should return the correct result")
    }
}